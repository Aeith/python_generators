import string


def g():
    """
    Generate all lowercase letters
    :return: lowercase letters
    """
    for i in list(string.ascii_lowercase):
        yield i


def g2(count=2):
    """
    Return count times the returns of the g generator
    :param count: n repetitions of g returns
    :return: n*g()
    """
    g1 = g()
    for i in g1:
        result = i
        for x in range(count-1):
            result += i

        yield result


# Generator simple
default = g2()
for v in default:
    print(v)

# Generator complex
test = g2(5)
for v in test:
    print(v)

