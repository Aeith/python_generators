

def chiffrePorteBonheur(nb):
    """
    Define if the number sent if a luck number or not
    :param nb: input number
    :return: bool True if one digit long
    """

    # Is a lucky number if the number is one
    if nb == "1":
        print("Le chiffre " + str(nb) + " est un porte bonheur")
        return True

    print("Le chiffre " + str(nb) + " n'est pas un porte bonheur")
    return False


def define_lucky_number(nb):
    """
    Recursive method to get lucky number
    :param nb: input number
    :return: new lucky number
    """

    print("Nombre de départ : ", nb)

    lucky = nb
    while len(lucky) > 1:
        new_lucky = 0
        for x in lucky:
            new_lucky += int(x)**2
            print(str(x) + "²=" + str(int(x)**2))

        lucky = str(new_lucky)
        print("Nouveau : ", lucky)

    return chiffrePorteBonheur(lucky)


lucky_number = define_lucky_number(input("Entrez un nombre : "))
