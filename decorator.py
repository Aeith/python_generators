def counting_decorator(function):
    """
    Decorator for power set function
    :param function: decorated function
    :return:
    """

    def sub_method(*args, **kwargs):
        """
        Update count after executing method
        :param args:
        :param kwargs:
        :return:
        """

        sub_method.calls += 1
        return function(*args, **kwargs)

    sub_method.calls = 0
    return sub_method


@counting_decorator
def powerset(power_set=[]):
    """
    Return the power set of the set in parameter
    :param power_set: set
    :return: power set of the set
    """

    # Recursive call
    if len(power_set) > 0:
        # Remove the first element for the next yielding
        for x in powerset(power_set[1::]):
            # Sub set
            yield x
            # First element + sub set
            yield [power_set[0]] + x
    # Empty set by default
    else:
        yield[]


liste = powerset([1, 2, 3, 4, 5, 6, 7, 8])
for e in liste:
    print(e)

print("Décorateur appelé " + str(powerset.calls) + " fois")
